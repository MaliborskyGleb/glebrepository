<?php

namespace RSS\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accounting
 *
 * @ORM\Table(name="accounting")
 * @ORM\Entity(repositoryClass="RSS\NewsBundle\Entity\AccountingRepository")
 */
class Accounting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datacreate", type="date", nullable=true)
     */
    private $datacreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datachange", type="date", nullable=true)
     */
    private $datachange;

    /**
     * @var integer
     *
     * @ORM\Column(name="visitors", type="integer", nullable=true)
     */
    private $visitors;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;

    public function __construct(){

        $this->datacreate = date_create();
        $this->datachange = date_create();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Accounting
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Accounting
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set datacreate
     *
     * @param \DateTime $datacreate
     * @return Accounting
     */
    public function setDatacreate($datacreate)
    {
        $this->datacreate = $datacreate;
    
        return $this;
    }

    /**
     * Get datacreate
     *
     * @return \DateTime 
     */
    public function getDatacreate()
    {
        return $this->datacreate;
    }

    /**
     * Set datachange
     *
     * @param \DateTime $datachange
     * @return Accounting
     */
    public function setDatachange($datachange)
    {
        $this->datachange = $datachange;
    
        return $this;
    }

    /**
     * Get datachange
     *
     * @return \DateTime 
     */
    public function getDatachange()
    {
        return $this->datachange;
    }

    /**
     * Set visitors
     *
     * @param integer $visitors
     * @return Accounting
     */
    public function setVisitors($visitors)
    {
        $this->visitors = $visitors;
    
        return $this;
    }

    /**
     * Get visitors
     *
     * @return integer 
     */
    public function getVisitors()
    {
        return $this->visitors;
    }

    /**
     * Set categoryid
     *
     * @param \RSS\NewsBundle\Entity\Category $categoryid
     * @return Accounting
     */
    public function setCategoryid(\RSS\NewsBundle\Entity\Category $categoryid = null)
    {
        $this->categoryid = $categoryid;
    
        return $this;
    }

    /**
     * Get categoryid
     *
     * @return \RSS\NewsBundle\Entity\Category 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }


}