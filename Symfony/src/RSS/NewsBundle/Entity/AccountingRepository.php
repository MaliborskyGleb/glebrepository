<?php

namespace RSS\NewsBundle\Entity;

use RSS\NewsBundle\Entity\Entity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;



class AccountingRepository extends EntityRepository
{
    public function findAccounting( $em, $search, $dateFrom, $dateTo, $sort)
    {
        $qb = $em->getRepository('RSSNewsBundle:Accounting')->createQueryBuilder('n');
        $qb->select('n');
        if (isset($search))  $qb->where($qb->expr()->like('n.name',$qb->expr()->literal('%'.$search .'')));
        if ($dateFrom) $qb->andWhere($qb->expr()->gte('n.datachange', $qb->expr()->literal($dateFrom->format('Y-m-d'))));
        if ($dateTo) $qb->andWhere($qb->expr()->lte('n.datachange',  $qb->expr()->literal($dateTo->format('Y-m-d'))));
        if ($sort) $qb->orderBy('n.'.$sort,'DESC');
        return $qb;
    }

}
//$qb->where('n.name = :name')->setParameter('name', $search);