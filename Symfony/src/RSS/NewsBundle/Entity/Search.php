<?php
namespace RSS\NewsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Search
{
    /**
     * @var string
     */
    public $search;

    /**
     * @var string
     */
    public $sort;

    /**
     * @var DateTime
     * @Assert\DateTime
     */
    public $dateFrom;

    /**
     * @var DateTime
     * @Assert\DateTime
     */
    public $dateTo;
}