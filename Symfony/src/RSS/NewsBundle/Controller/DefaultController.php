<?php

namespace RSS\NewsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use RSS\NewsBundle\Entity\Search as Search;
use RSS\NewsBundle\Form\SearchType as SearchType;

class DefaultController extends Controller
{
    /**
     * Finds and displays a Accounting entity.
     *
     * @Route("/news/{id}", name="url_show")
     * Method("GET|POST|PUT")
     * @Template()
     */
    public function indexAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $entities=$em->getRepository('RSSNewsBundle:Accounting')->find($id);
        $url=$entities->getUrl();
        if(!isset($_GET["page"]))$entities->setVisitors($entities->getVisitors()+1);
        $em->flush();
        $rss=simplexml_load_file($url);
        $news=$rss->xpath('//item');
        $paginator=$this->get('knp_paginator');
        $pagination=$paginator->paginate($news,
            $this->get('request')->query->get('page',1),10);
        return array(
            'rss'=>$pagination,
        );
    }

    /**
     * @Route("/main", name="main")
     * Method("GET")
     * @Template()
     */
    public function newsAction()
    {
        $search = new Search();
        $form = $this->createForm(new SearchType, $search);
        $form->submit($this->getRequest());
        $em=$this->getDoctrine()->getManager();
        $entities=$em->getRepository('RSSNewsBundle:Accounting');
        $engine=$em->getRepository('RSSNewsBundle:Accounting')->findAll();
        $qb=$entities->findAccounting($em, $search->search, $search->dateFrom, $search->dateTo, $search->sort);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $qb->getQuery(),
            $this->get('request')->query->get('page', 1), 5);
        return array(
            'entities' => $pagination,'search_form' => $form->createView(),
            'engine' =>$engine
        );
    }
}
