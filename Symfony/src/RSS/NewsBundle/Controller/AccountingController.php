<?php

namespace RSS\NewsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use RSS\NewsBundle\Entity\Accounting;
use RSS\NewsBundle\Form\AccountingType;
use Rss\NewsBundle\Entity\Search as Search;
use Rss\NewsBundle\Form\SearchType as SearchType;

/**
 * Accounting controller.
 *
 * @Route("/accounting")
 */
class AccountingController extends Controller
{

    /**
     * Lists all Accounting entities.
     *
     * @Route("/", name="accounting")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RSSNewsBundle:Accounting')->findAll();
        return array(
            'entities' => $entities,
        );
    }

    public function check($file)
    {
        libxml_use_internal_errors(true);
        if(@$rss = simplexml_load_file($file)){
            if($rss->xpath('//rss') && $rss->xpath('//channel') &&
                $rss->xpath('//item') && $rss->xpath('//title') &&
                $rss->xpath('//description') && $rss->xpath('//link')) return true;}
        else {
            $this->get('session')->getFlashbag()->add(
                'error_message',
                'RssLink is bad.'
            );return false;}
    }

    /**
     * Creates a new Accounting entity.
     *
     * @Route("/", name="accounting_create")
     * @Method("POST")
     * @Template("RSSNewsBundle:Accounting:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Accounting();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);



        if ($form->isValid()&& $this->check($entity->getUrl())) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashbag()->add(
                'success_message',
                'RssLink is added.'
            );
            return $this->redirect($this->generateUrl('accounting_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Accounting entity.
     *
     * @param Accounting $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Accounting $entity)
    {
        $form = $this->createForm(new AccountingType(), $entity, array(
            'action' => $this->generateUrl('accounting_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Accounting entity.
     *
     * @Route("/new", name="accounting_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Accounting();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Accounting entity.
     *
     * @Route("/{id}", name="accounting_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        print_r($id);
        $entity = $em->getRepository('RSSNewsBundle:Accounting')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Accounting entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Accounting entity.
     *
     * @Route("/{id}/edit", name="accounting_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RSSNewsBundle:Accounting')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Accounting entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Accounting entity.
     *
     * @param Accounting $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Accounting $entity)
    {
        $form = $this->createForm(new AccountingType(), $entity, array(
            'action' => $this->generateUrl('accounting_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Accounting entity.
     *
     * @Route("/{id}", name="accounting_update")
     * @Method("PUT")
     * @Template("RSSNewsBundle:Accounting:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RSSNewsBundle:Accounting')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Accounting entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashbag()->add(
                'success_message',
                'Rss update.');
            return $this->redirect($this->generateUrl('accounting_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Accounting entity.
     *
     * @Route("/{id}", name="accounting_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RSSNewsBundle:Accounting')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Accounting entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashbag()->add(
                'delete_message',
                'Rss delete.');
        }

        return $this->redirect($this->generateUrl('accounting'));
    }

    /**
     * Creates a form to delete a Accounting entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accounting_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
