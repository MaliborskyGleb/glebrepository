<?php

/* RSSNewsBundle:Security:login.html.twig */
class __TwigTemplate_ad3bcabd2479e842785a8ebd12ea3c876d70440f613eca193b9c2a1ed1eff697 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3><legend> Авторизация </legend></h3>
";
        // line 2
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "



";
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
    ";
        // line 4
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 5
            echo "        <div class=\"alert alert-error\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "message"), "html", null, true);
            echo "</div>
    ";
        }
        // line 7
        echo "
<form action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" placeholder=\"Введите логин\" id=\"username\" name=\"_username\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
    <label for=\"password\">Password:</label>
    <input type=\"password\" placeholder=\"Введите пароль\" id=\"password\" name=\"_password\" />
    <button class=\"btn btn-inverse\" type=\"submit\">Log in</button>
    </form>
";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Security:login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  55 => 10,  50 => 8,  47 => 7,  41 => 5,  39 => 4,  36 => 3,  33 => 2,  25 => 16,  23 => 2,  20 => 1,);
    }
}
