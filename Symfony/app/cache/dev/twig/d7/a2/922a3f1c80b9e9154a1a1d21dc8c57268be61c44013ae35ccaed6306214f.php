<?php

/* ::base.html.twig */
class __TwigTemplate_d7a2922a3f1c80b9e9154a1a1d21dc8c57268be61c44013ae35ccaed6306214f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <title>
        ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "    </title>
    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 11
        echo "</head>
<body>
<div class=\"container\">
    ";
        // line 14
        $this->displayBlock('header', $context, $blocks);
        // line 15
        echo "    ";
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "    ";
        $this->displayBlock('footer', $context, $blocks);
        // line 17
        echo "</div>
</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\">
    ";
    }

    // line 10
    public function block_javascripts($context, array $blocks = array())
    {
    }

    // line 14
    public function block_header($context, array $blocks = array())
    {
    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
    }

    // line 16
    public function block_footer($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 15,  80 => 14,  75 => 10,  68 => 8,  60 => 5,  54 => 17,  51 => 16,  48 => 15,  46 => 14,  41 => 11,  38 => 10,  36 => 7,  33 => 6,  31 => 5,  25 => 1,  121 => 49,  113 => 44,  106 => 39,  94 => 33,  90 => 16,  83 => 28,  77 => 27,  73 => 26,  69 => 24,  65 => 7,  52 => 12,  49 => 9,  43 => 7,  39 => 6,  35 => 5,  32 => 4,  29 => 3,);
    }
}
