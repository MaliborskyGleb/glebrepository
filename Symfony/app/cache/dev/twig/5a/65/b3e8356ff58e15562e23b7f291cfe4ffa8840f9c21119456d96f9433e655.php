<?php

/* RSSNewsBundle:Accounting:new.html.twig */
class __TwigTemplate_5a65b3e8356ff58e15562e23b7f291cfe4ffa8840f9c21119456d96f9433e655 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Accounting creation</h1>


    ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "error_message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 8
            echo "            ";
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "

    ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

<a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("main");
        echo "\">
    Back to the list
    </a>

    <div id=\"les10_ex1\" class=\"btn\">Перетащи меня</div>
<form>
        <INPUT type=\"text\" id=\"les_x2\" SIZE=20 MAXLEGNTH=20>
</form>
<div id=\"result\"></div>

";
    }

    // line 26
    public function block_script($context, array $blocks = array())
    {
        // line 27
        echo "    <link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css\" />
    <script src=\"http://code.jquery.com/jquery-1.9.1.js\"></script>
    <script src=\"http://code.jquery.com/ui/1.10.3/jquery-ui.js\"></script>
    <script type=\"text/javascript\">
        \$(document).ready(function(){
            \$(\"#les10_ex1\").draggable({containment:'document',
            start:function(){
                contents=\$(this).text();
            }
            });
            \$(\"#rss_newsbundle_accounting_name\").droppable({accept:\"#les10_ex1\",
                drop:function()
                {
\$(\"#rss_newsbundle_accounting_name\").val(function()
        {
            return contents;
        }
)
                }
            } );
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Accounting:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 27,  74 => 26,  59 => 14,  54 => 12,  50 => 10,  41 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}
