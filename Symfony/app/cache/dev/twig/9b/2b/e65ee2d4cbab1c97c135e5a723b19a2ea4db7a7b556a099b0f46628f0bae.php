<?php

/* RSSNewsBundle:Category:new.html.twig */
class __TwigTemplate_9b2be65ee2d4cbab1c97c135e5a723b19a2ea4db7a7b556a099b0f46628f0bae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Category creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("main");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>

<div class=\"btn-group\">
    <div id=\"les10_ex1\" class=\"btn\">Elections</div>
    <div id=\"les10_ex1\" class=\"btn\">Policy</div>
    <div id=\"les10_ex1\" class=\"btn\">Finance</div>
    <div id=\"les10_ex1\" class=\"btn\">Society</div>
    <div id=\"les10_ex1\" class=\"btn\">Sport</div>
    <div id=\"les10_ex1\" class=\"btn\">Culture</div>
    <div id=\"les10_ex1\" class=\"btn\">Auto</div>
    <div id=\"les10_ex1\" class=\"btn\">Games</div>
<div>


";
    }

    // line 31
    public function block_script($context, array $blocks = array())
    {
        // line 32
        echo "    <link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css\" />
    <script src=\"http://code.jquery.com/jquery-1.9.1.js\"></script>
    <script src=\"http://code.jquery.com/ui/1.10.3/jquery-ui.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/ui.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Category:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 35,  69 => 32,  66 => 31,  43 => 10,  36 => 6,  32 => 4,  29 => 3,);
    }
}
