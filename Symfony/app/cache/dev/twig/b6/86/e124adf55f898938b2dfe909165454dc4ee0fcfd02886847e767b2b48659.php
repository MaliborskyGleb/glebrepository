<?php

/* RSSNewsBundle:Default:news.html.twig */
class __TwigTemplate_b686e124adf55f898938b2dfe909165454dc4ee0fcfd02886847e767b2b48659 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "
    <br> <a href=\"";
        // line 5
        echo "login";
        echo "\" class=\"btn\">login</a>
    <a href=\"";
        // line 6
        echo "logout";
        echo "\" class=\"btn\">logout</a>
    <a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("category_new");
        echo "\" class=\"btn\">category</a>

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<div id=\"item\">
        <ul>
            ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 16
            echo "                <li>
                    <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("url_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "name"), "html", null, true);
            echo "</a>
                </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </ul>
    </div>

    <br>
    <br>
    <form action=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("main");
        echo "\" method=\"get\" class=\"form-inline\" role=\"form\">
        <div class=\"row\">
            <div class=\"span3 thumb-list\">
                ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["search_form"]) ? $context["search_form"] : $this->getContext($context, "search_form")), "search"), 'row');
        echo "
            </div>
            <div class=\"span3 thumb-list\">
                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["search_form"]) ? $context["search_form"] : $this->getContext($context, "search_form")), "dateFrom"), 'row');
        echo "
            </div>
            <div class=\"span3 thumb-list\">
                ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["search_form"]) ? $context["search_form"] : $this->getContext($context, "search_form")), "dateTo"), 'row');
        echo "
            </div>
            <div class=\"span3 thumb-list\">
                ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["search_form"]) ? $context["search_form"] : $this->getContext($context, "search_form")), "sort"), 'row');
        echo "
            </div>
            <div class=\"span3 thumb-list\"><br>

                <button type=\"submit\" class=\"btn btn-default\">Search</button>
            </div>
        </div>
    </form>

    <h1>Accounting list</h1>
    <table class=\"table\">
        <thead>
        <tr>
            <th>Name</th>
            <th>Visitors</th>
            <th>Category</th>
            ";
        // line 53
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 54
            echo "                <th>Url</th>
            ";
        }
        // line 56
        echo "        </tr>
        </thead>
        <tbody>
        ";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 60
            echo "            <tr>
                <td ><a href=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("url_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "name"), "html", null, true);
            echo "</a></td>
                <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "visitors"), "html", null, true);
            echo "</td>
                <td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "categoryid"), "html", null, true);
            echo "</td>
                ";
            // line 64
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "url"), "html", null, true);
                echo "</td>
                <td>
                    <div class=\"btn-group\">
                            <a href=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("accounting_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" class=\"btn\">show</a>
                            <a href=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("accounting_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\"class=\"btn\">edit</a>
                    </div>
                </td>
                ";
            }
            // line 73
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "        </tbody>
    </table>
";
        // line 77
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 78
            echo "    <ul>
        <li>
            <a href=\"";
            // line 80
            echo $this->env->getExtension('routing')->getPath("accounting_new");
            echo "\"class=\"btn\">
                Create a new entry
            </a>
        </li>
    </ul>
";
        }
        // line 86
        echo "    ";
        echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        echo "
";
    }

    // line 89
    public function block_script($context, array $blocks = array())
    {
        // line 90
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/jquery-1.3.2.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/3DEngine.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
    <script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/Ring.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
    <script src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/func.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Default:news.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 93,  223 => 92,  219 => 91,  214 => 90,  211 => 89,  204 => 86,  195 => 80,  191 => 78,  189 => 77,  185 => 75,  178 => 73,  171 => 69,  167 => 68,  160 => 65,  158 => 64,  154 => 63,  150 => 62,  144 => 61,  141 => 60,  137 => 59,  132 => 56,  128 => 54,  126 => 53,  107 => 37,  101 => 34,  95 => 31,  89 => 28,  83 => 25,  76 => 20,  65 => 17,  62 => 16,  58 => 15,  54 => 13,  51 => 11,  44 => 7,  40 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
