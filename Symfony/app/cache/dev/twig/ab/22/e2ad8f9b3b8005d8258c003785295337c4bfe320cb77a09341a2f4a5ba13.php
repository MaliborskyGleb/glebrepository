<?php

/* RSSNewsBundle:Default:index.html.twig */
class __TwigTemplate_ab22e2ad8f9b3b8005d8258c003785295337c4bfe320cb77a09341a2f4a5ba13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_header($context, array $blocks = array())
    {
        // line 3
        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("main");
        echo "\">
    Back to the list

 ";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "<hr>
    ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rss"]) ? $context["rss"] : $this->getContext($context, "rss")));
        foreach ($context['_seq'] as $context["_key"] => $context["entities"]) {
            // line 10
            echo "        <div class=\"row\">
            <div class=\"span12 thumb-list\">
                <h4><a href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "link"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "title"), "html", null, true);
            echo "</a></h4>
                <p>";
            // line 13
            echo $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "description");
            echo "</p>
                <p>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "pubDate"), "html", null, true);
            echo " </p>
            </div>
        </div>
        <hr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entities'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "    ";
        echo $this->env->getExtension('knp_pagination')->render((isset($context["rss"]) ? $context["rss"] : $this->getContext($context, "rss")));
        echo "

";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 19,  65 => 14,  61 => 13,  55 => 12,  51 => 10,  47 => 9,  44 => 8,  41 => 7,  32 => 3,  29 => 2,);
    }
}
