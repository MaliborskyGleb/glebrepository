<?php

/* RSSNewsBundle:Default:news.html.twig */
class __TwigTemplate_307f80375d29c7185b87ff3dbea8d10febb95e2108975a02da2d9b7d5bbb37ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "    <br> <a href=\"";
        echo "login";
        echo "\" class=\"btn\">login</a>
    <a href=\"";
        // line 5
        echo "logout";
        echo "\" class=\"btn\">logout</a>
";
        // line 6
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 7
            echo "<a href=\"";
            echo $this->env->getExtension('routing')->getPath("category_new");
            echo "\" class=\"btn\">category</a>
";
        }
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<h1>Accounting list</h1>

    <table class=\"table\">
        <thead>
        <tr>
            <th>Name</th>
            <th>Visitors</th>
            ";
        // line 20
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 21
            echo "                <th>Url</th>
            ";
        }
        // line 23
        echo "        </tr>
        </thead>
        <tbody>
        ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 27
            echo "            <tr>
                <td ><a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("url_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "name"), "html", null, true);
            echo "</a></td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "visitors"), "html", null, true);
            echo "</td>
                ";
            // line 30
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 31
                echo "                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "url"), "html", null, true);
                echo "</td>
                <td>
                    <div class=\"btn-group\">
                            <a href=\"";
                // line 34
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("accounting_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" class=\"btn\">show</a>
                            <a href=\"";
                // line 35
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("accounting_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\"class=\"btn\">edit</a>
                    </div>

                </td>
                ";
            }
            // line 40
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "
        </tbody>
    </table>
";
        // line 45
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 46
            echo "    <ul>
        <li>
            <a href=\"";
            // line 48
            echo $this->env->getExtension('routing')->getPath("accounting_new");
            echo "\"class=\"btn\">
                Create a new entry
            </a>
        </li>
    </ul>
";
        }
        // line 54
        echo "    ";
        echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        echo "
";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Default:news.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 54,  130 => 48,  126 => 46,  124 => 45,  119 => 42,  112 => 40,  104 => 35,  100 => 34,  93 => 31,  91 => 30,  87 => 29,  81 => 28,  78 => 27,  74 => 26,  69 => 23,  65 => 21,  63 => 20,  54 => 13,  51 => 10,  43 => 7,  41 => 6,  37 => 5,  32 => 4,  29 => 3,);
    }
}
