<?php

/* RSSNewsBundle::Default/layout.html.twig */
class __TwigTemplate_0fae4e7099c481648d6565759d1e0c3eee5827c447547161f548b3db5069c8f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('body', $context, $blocks);
    }

    public function block_body($context, array $blocks = array())
    {
        // line 2
        echo "<div id=\"container\">
    <header class=\"clearfix\">
        <h1>
            symfony2 Учебник
        </h1>
        <nav>
            <ul>
                ";
        // line 9
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 10
            echo "                    <li>
                        <a href=\"";
            // line 11
            echo $this->env->getExtension('routing')->getPath("_security_logout");
            echo "\">
                            Выход
                        </a>
                    </li>
                ";
        }
        // line 16
        echo "            </ul>
        </nav>
    </header>
    ";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle::Default/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  48 => 16,  40 => 11,  37 => 10,  35 => 9,  26 => 2,  20 => 1,);
    }
}
