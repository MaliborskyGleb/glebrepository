<?php

/* ::base.html.twig */
class __TwigTemplate_6832ff42cec9ae595968938477dccf791ba0d1b362a733ee11fae9749c819297 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <title>
        ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "    </title>
    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 11
        echo "</head>
<body>
<div class=\"container\">
    ";
        // line 14
        $this->displayBlock('header', $context, $blocks);
        // line 15
        echo "    ";
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "    ";
        $this->displayBlock('footer', $context, $blocks);
        // line 17
        echo "</div>
</body>
";
        // line 19
        $this->displayBlock('script', $context, $blocks);
        // line 20
        echo "</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/css/bootstrap.css"), "html", null, true);
        echo "\">
    ";
    }

    // line 10
    public function block_javascripts($context, array $blocks = array())
    {
    }

    // line 14
    public function block_header($context, array $blocks = array())
    {
    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
    }

    // line 16
    public function block_footer($context, array $blocks = array())
    {
    }

    // line 19
    public function block_script($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  100 => 19,  95 => 16,  90 => 15,  85 => 14,  80 => 10,  73 => 8,  70 => 7,  65 => 5,  61 => 20,  59 => 19,  55 => 17,  52 => 16,  49 => 15,  47 => 14,  42 => 11,  39 => 10,  37 => 7,  34 => 6,  32 => 5,  26 => 1,);
    }
}
