<?php

/* RSSNewsBundle:Default:index.html.twig */
class __TwigTemplate_a5cf10b08e8b19cdd0db178d6c72718688ea4158491d9e27e96db191f726a8cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<hr>
    ";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rss"]) ? $context["rss"] : $this->getContext($context, "rss")));
        foreach ($context['_seq'] as $context["_key"] => $context["entities"]) {
            // line 6
            echo "        <div class=\"row\">
            <div class=\"span12 thumb-list\">
                <h4><a href=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "link"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "title"), "html", null, true);
            echo "</a></h4>
                <p>";
            // line 9
            echo $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "description");
            echo "</p>
                <p>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), "pubDate"), "html", null, true);
            echo " </p>
            </div>
        </div>
        <hr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entities'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "    ";
        echo $this->env->getExtension('knp_pagination')->render((isset($context["rss"]) ? $context["rss"] : $this->getContext($context, "rss")));
        echo "
";
    }

    public function getTemplateName()
    {
        return "RSSNewsBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 15,  52 => 10,  48 => 9,  42 => 8,  38 => 6,  34 => 5,  31 => 4,  28 => 3,);
    }
}
